#Source https://github.com/carlossg/docker-maven/blob/40cbcd2edc2719c64062af39baac6ae38d0becf9/jdk-7/Dockerfile
#Specify java version
FROM java:openjdk-7-jdk
#Specify maven vesion
ENV MAVEN_VERSION 3.3.9

#Install maven
RUN mkdir -p /usr/share/maven \
  && curl -fsSL http://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \
    | tar -xzC /usr/share/maven --strip-components=1 \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven

#Add code to folder
ADD . /app

#
WORKDIR /app

#Maven package
RUN mvn clean package

#Run jar
CMD ["java", "-jar", "/app/target/anagramValidator-0.0.1-SNAPSHOT.jar"]
