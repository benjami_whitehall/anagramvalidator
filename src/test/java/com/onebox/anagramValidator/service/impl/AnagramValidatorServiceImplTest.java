/**
 * 
 */
package com.onebox.anagramValidator.service.impl;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * @author Usuari
 *
 */
public class AnagramValidatorServiceImplTest {

    private static String VERSE_SIMPLE = "This is a verse";
    private static String VERSE_WITH_ACCENTS = "This is a vérsê";
    private static String VERSE_WITH_NON_ALPHABET_CHAR = "This,is a vérsê.";
    
    private static String ANAGRAM_POEM = "I like drama eaten in.\n Lake air. I need a mint.\n I like a dinner, a tame\n Ariel diet, a.k.a., in men.";
    private static String NON_ANAGRAM_POEM = "This is not a anagram poem\n no way it is\n lines do not match at all";
    private static String SINGLE_LINE_POEM = "Can a single line be a poem?";

    private static String VERSE_OK_NORMALIZED = "thisisaverse";

    private AnagramValidatorServiceImpl anagramValidatorServiceImpl;

    @Before
    public void setUp() {
	anagramValidatorServiceImpl = new AnagramValidatorServiceImpl();
    }
    
    /**
     *  Test a simple verse
     */
    @Test
    public void normalizeVerseTestOK(){
	String normalizedVerse = anagramValidatorServiceImpl.normalizeVerse(VERSE_SIMPLE);
	assertEquals(VERSE_OK_NORMALIZED, normalizedVerse);
    }
    
    /**
     * Test a verse with accents
     */
    @Test
    public void normalizeVerseTestWithAccents(){
	String normalizedVerse = anagramValidatorServiceImpl.normalizeVerse(VERSE_WITH_ACCENTS);
	assertEquals(VERSE_OK_NORMALIZED, normalizedVerse);
    }
    
    /**
     * Test a verse with non alphabetical characters
     */
    @Test
    public void normalizeVerseTestWithNonAlphabetical(){	
	String normalizedVerse = anagramValidatorServiceImpl.normalizeVerse(VERSE_WITH_NON_ALPHABET_CHAR);
	assertEquals(VERSE_OK_NORMALIZED, normalizedVerse);	
    }
    
    /**
     * Test correct behavior with anagram poem
     */
    @Test
    public void isAnagramPoemTestOK(){	
	assertTrue(anagramValidatorServiceImpl.isAnagramPoem(ANAGRAM_POEM));
    }
    
    /**
     * Test correct behavior with a non anagram poem
     */
    @Test
    public void isAnagramPoemTestKO(){	
	assertFalse(anagramValidatorServiceImpl.isAnagramPoem(NON_ANAGRAM_POEM));
    }
    
    
    /**
     * Test correct behavior with single line
     */    
    @Test
    public void isAnagramPoemTestSingleLine(){	
	assertFalse(anagramValidatorServiceImpl.isAnagramPoem(SINGLE_LINE_POEM));
    }
}
