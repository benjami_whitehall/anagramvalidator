/**
 * 
 */
package com.onebox.anagramValidator.service.impl;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.onebox.anagramValidator.service.AnagramValidatorService;

/**
 * @author benjami.cantos
 *
 */
@Service
public class AnagramValidatorServiceImpl implements AnagramValidatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnagramValidatorServiceImpl.class);

    @Override
    public boolean isAnagramPoem(String pPoem) {
	boolean isAnagramPoem = false;
	if (pPoem != null && !pPoem.isEmpty()) {

	    //Split poem into verses
	    String[] verses = pPoem.split("\n");
	    LOGGER.debug("Poem has "+verses.length+" verses");
	    //Check if it's a single line poem
	    if (verses.length > 1) {
		int i = 0;
		// Create a has to contain a hash of numbers of each letter per verse
		Map<Integer, Map<String, Integer>> verseHash = new HashMap<Integer, Map<String, Integer>>();
		for (String verse : verses) {
		    if (i <= 1 || isAnagramPoem ){
			String[] characters = normalizeVerse(verse).split("(?!^)");
			Map<String, Integer> characterCounter = new HashMap<String, Integer>();
			for (String character : characters) {
			    if (characterCounter.containsKey(character)) {
				Integer charCount = characterCounter.get(character);
				characterCounter.put(character, charCount + 1);
			    } else {
				characterCounter.put(character, 1);
			    }
			}
			
			// Do not do if it's first iteration
			if (i != 0) {
			    // Compare previous with previous map has to check if
			    // has the same number of letters
			    Map<String, Integer> prevCharCounter = verseHash.get(i - 1);
			    if (!prevCharCounter.equals(characterCounter)) {
				LOGGER.info("Distinct verses have been found, not an anagram poem");
				isAnagramPoem = false;
			    } else {
				isAnagramPoem = true;
			    }
			}
			verseHash.put(i, characterCounter);			
			i++;
		    }else{
			//Different verse found so it's not an anagram poem
			break;
		    }
		}

	    }else{
		LOGGER.info("Single line poem can't be an anagram");
	    }
	}
	return isAnagramPoem;
    }

    /**
     * @param verse
     * @return normalized text removing all non alphabetical characters and
     *         spaces
     */
    public String normalizeVerse(final String pVerse) {
	String verse = "";
	if (!pVerse.isEmpty()) {
	    // Remove all Spaces an lowercase All
	    verse = pVerse.replaceAll("\\s+", "").toLowerCase();
	    // normalize text
	    verse = Normalizer.normalize(verse, Normalizer.Form.NFD);
	    // Replace all non alphabetical characters
	    verse = verse.replaceAll("[^a-zA-Z]", "");
	}
	return verse;

    }

}
