/**
 * 
 */
package com.onebox.anagramValidator.service;

import org.springframework.stereotype.Service;

/**
 * @author bcantos
 *
 */
@Service
public interface AnagramValidatorService {
    
    /**
     * @param pPoem
     * @return if all verses in poem are anagrams of the other verses
     */
    public boolean isAnagramPoem(String pPoem);

}
