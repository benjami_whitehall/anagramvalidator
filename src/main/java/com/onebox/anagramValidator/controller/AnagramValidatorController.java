/**
 * 
 */
package com.onebox.anagramValidator.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onebox.anagramValidator.service.AnagramValidatorService;

/**
 * @author benjami.cantos
 *
 */
@RestController
public class AnagramValidatorController {

    @Autowired
    AnagramValidatorService anagramValidatorService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AnagramValidatorController.class);

    /**
     * @param pFileName
     * @return Read poem from a file
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST)
    @ResponseBody
    public boolean readPoemFormFile(@RequestBody @NotNull String pFileName) {
	String poem;
	try {
	    poem = readPoemFromFile(pFileName);
	} catch (IOException e) {
	    LOGGER.error("Error while loading file " + e.getMessage());
	    throw new IllegalArgumentException("File not found " + pFileName);
	}
	return anagramValidatorService.isAnagramPoem(poem);
    }

    /**
     * @param pPoem
     * @return
     * Read poem form POST body
     */
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    @ResponseBody
    public boolean readPoemFormBody(@RequestBody @NotNull String pPoem) {
	return anagramValidatorService.isAnagramPoem(pPoem);
    }

    private String readPoemFromFile(String pPath) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(pPath));
	return org.apache.commons.io.IOUtils.toString(br);

    }

}
